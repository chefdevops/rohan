default['gitlab-setup'] ['prereq_packages'] = %w(curl openssh-server postfix firewalld)
default['gitlab-setup'] ['enable_start_services'] = %w(sshd postfix firewalld)
default['gitlab-setup'] ['firewall_rules'] = %w(80 443)

