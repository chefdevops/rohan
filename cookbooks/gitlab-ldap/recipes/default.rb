#
# Cookbook:: gitlab-ldap
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

Chef::Log.fatal("node['platform_family']: #{node['platform_family']}")

node['gitlab-setup']['prereq_packages'].each do |pkg| 
    Chef::Log.fatal("node['gitlab-setup']['prereq_packages']: #{pkg}")
    package pkg
end		

node['gitlab-setup']['enable_start_services'].each do |srv|
    Chef::Log.fatal("node['gitlab-setup']['enable_start_services']: #{srv}")
    service srv do
        action [:enable, :start]
    end
end

node['gitlab-setup'] ['firewall_rules'].each do |port|
    Chef::Log.fatal("node['gitlab-setup']['firewall_rules']: #{port}")
    firewalld_port "#{port}/tcp" do
        action :add
        zone 'public'
    end
end

yum_repository 'gitlab_gitlab-ce' do
  description "GITLAB yum repo"
  baseurl "https://packages.gitlab.com/gitlab/gitlab-ce/el/7/$basearch"
  #repo_gpgcheck 'true'
  #gpgcheck 'false'
  gpgkey 'https://packages.gitlab.com/gitlab/gitlab-ce/gpgkey'
  #enabled 'true'
  #sslverify 'true'
  action :create
end

yum_package 'gitlab-ce' do
  options '--nogpgcheck'
  version '8.16.2-ce.0.el7'
end  

bash 'reconfigure gitlab' do
  code <<-EOH 
     gitlab-ctl reconfigure
  EOH
end
